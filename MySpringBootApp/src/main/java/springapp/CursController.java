package springapp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class CursController {
	
	@Autowired
	private CursRepository repository;
	
	@RequestMapping("/")
	public String index(Model model){
		List<Curs> courses = (List<Curs>) repository.findAll();
		model.addAttribute("courses",courses);
		return "indexCurs";
	}
		
		@RequestMapping(value="addCurs")
		public String addCurs(Model model){
			
			model.addAttribute("curs",new Curs());
			return "addCurs";
		}
		@RequestMapping(value="saveCurs", method = RequestMethod.POST)
		public String save(Curs curs){
			repository.save(curs);
			return "redirect:/";
		}
		@RequestMapping(value="/deleteCurs/{id}",method=RequestMethod.GET)
		public String deleteCurs(@PathVariable("id")Long cursId, Model model){
			repository.deleteById(cursId);
			return "redirect:/";
		}
		@RequestMapping(value="/addStudentCurs/{cursId}",method=RequestMethod.GET)
		public String addStudentCurs(@PathVariable("cursId")Long cursId, Model model){
			return "redirect:/students/"+cursId;
		}


}
