package springapp;

import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class StudentController {
	@Autowired
	private StudentRepository repository;
	@Autowired
	private CursRepository cursRepository;
	@RequestMapping("/students/{cursId}")
	public String index(@PathVariable("cursId") Long cursId,Model model){
		Curs curs=cursRepository.findById(cursId).get();
		List<Student> students=curs.getStudents();
		Comparator<Student> byfirstName = new Comparator<Student>() {
			 public int compare(Student d1, Student d2) {
				 int i=d1.getFirstName().compareTo(d2.getFirstName());
				 if(i!=0) return i;
				 return d1.getLastName().compareTo(d2.getLastName());
			 }
		};
		Collections.sort(students,byfirstName);
		model.addAttribute("students",students);
		model.addAttribute("cursId", cursId);
		return "index";
	}
	
	
	@RequestMapping(value="add/{cursId}")
	public String addStudent(@PathVariable("cursId") Long cursId,Model model){
		Curs curs=cursRepository.findById(cursId).get();
		Student student=new Student();
		student.setCurs(curs);
		model.addAttribute("student",student);
		model.addAttribute("cursId", cursId);
		return "addStudent";
	}
	@RequestMapping(value="add/save/{cursId}", method = RequestMethod.POST)
	public String save(@PathVariable("cursId") Long cursId,Student student){
		Curs curs=cursRepository.findById(cursId).get();
		student.setCurs(curs);
		curs.addStudent(student);
		repository.save(student);
		cursRepository.save(curs);
		return "redirect:/";
	}
	@RequestMapping(value="/delete/{id}",method=RequestMethod.GET)
	public String deleteStudent(@PathVariable("id")Long studentId, Model model){
		repository.deleteById(studentId);
		return "redirect:/";
	}

}
